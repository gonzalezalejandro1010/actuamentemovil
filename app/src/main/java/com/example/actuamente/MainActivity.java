package com.example.actuamente;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    EditText editNombre,editCorreo,editCelular,editCiudad;
    Button btnAgregar;
    private TextView mostrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editNombre=(EditText)findViewById(R.id.editNombre);
        editCorreo=(EditText)findViewById(R.id.editCorreo);
        editCelular=(EditText)findViewById(R.id.editCelular);
        editCiudad=(EditText)findViewById(R.id.editCiudad);
        btnAgregar=(Button)findViewById(R.id.btnAgregar);
        mostrar = (TextView)findViewById(R.id.mostrar) ;


        btnAgregar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {


                String dato = null;

                dato = "Gracias por enviar tus datos";
                mostrar.setText(dato);

                ejecutarServicio ("http://actuamente.sellu.co/controladores/controladorDatos.php");

            }
        });
    }

    private void ejecutarServicio(String URL){

        StringRequest stringRequest=new StringRequest(Request.Method.POST,URL,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(getApplicationContext(), "OPERACION EXITOSA", Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.toString(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String,String> parametros=new HashMap<String,String>();
                parametros.put("name",editNombre.getText().toString());
                parametros.put("email",editCorreo.getText().toString());
                parametros.put("celular",editCelular.getText().toString());
                parametros.put("ciudad",editCiudad.getText().toString());
                return parametros;
            }
        };
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }


}
